package com.surabi.userservice.service;


import com.surabi.userservice.entity.MenuEntity;

import java.util.List;

public interface IMenuService {

    //read
    List<MenuEntity> getAllMenuItems();
}
