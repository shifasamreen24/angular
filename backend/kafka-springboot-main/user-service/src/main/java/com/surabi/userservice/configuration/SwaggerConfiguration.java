package com.surabi.userservice.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket surabiUsersApi(){
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(info()).groupName("Surabi-Users-API")
                .select().apis(RequestHandlerSelectors.basePackage("com.surabi.userservice.controller"))
                .build();
    }

    public ApiInfo info(){
        return new ApiInfoBuilder()
                .title("Surabi Users API")
                .contact(new Contact("Restaurant API", "https://surabi-group.com", "surabi_restaurant@gmail.com"))
                .description("Surabi Restaurant Services")
                .termsOfServiceUrl("https://surabi-group.com/toc")
                .version("1.0.0")
                .license("Surabi Group License").licenseUrl("https://surabi-group.com/license")
                .build();
    }
}
