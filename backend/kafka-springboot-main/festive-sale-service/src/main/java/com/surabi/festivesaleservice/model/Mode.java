package com.surabi.festivesaleservice.model;

public enum Mode {
    CASH, CARD;
}
