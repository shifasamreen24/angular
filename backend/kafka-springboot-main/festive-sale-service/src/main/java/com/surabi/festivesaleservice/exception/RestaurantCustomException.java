package com.surabi.festivesaleservice.exception;

import lombok.*;
import org.springframework.http.HttpStatus;

import java.util.Date;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@ToString
public class RestaurantCustomException extends RuntimeException {
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
    private HttpStatus code;
    private String message;
    private Date timestamp;

    public RestaurantCustomException(String message, HttpStatus code){
        this.code=code;
        this.message=message;
    }
    public RestaurantCustomException(String message, HttpStatus code, Date timestamp){
        this.code=code;
        this.message=message;
        this.timestamp=timestamp;
    }
}
