package com.surabi.festivesaleservice.model;

import lombok.*;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Feedback {
//    @NotNull
//    @Min(0)
//    @Max(10)
    private Integer rating;
    private String comments;
}
