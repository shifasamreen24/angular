package com.surabi.offlineuserservice.service;



import com.surabi.offlineuserservice.entity.MenuEntity;

import java.util.List;

public interface IMenuService {

    //read
    List<MenuEntity> getAllMenuItems();


}
