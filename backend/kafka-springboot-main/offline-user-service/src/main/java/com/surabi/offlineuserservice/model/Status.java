package com.surabi.offlineuserservice.model;

public enum Status {
    UNPAID, PAID;
}
