# Kafka SpringBoot


## Topics

1. online-user-admin-topic - User service sends message to admin
2. offline-user-admin-topic - Offline user service sends message to admin asynchronously 
3. users-group-chat-topic  - Online users group chat in real-time

## Kafka Configuration in microservices

1. admin-service - Consumer configuration for online and offline user messages
2. offline-user-service - Producer configuration for admin chat.
3. user-service - Producer configuration for admin chat and users group chat. Consumer configuration for users group chat.

