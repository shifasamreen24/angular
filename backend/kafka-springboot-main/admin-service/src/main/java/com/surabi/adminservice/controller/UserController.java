package com.surabi.adminservice.controller;


import com.surabi.adminservice.entity.AuthoritiesEntity;
import com.surabi.adminservice.entity.UserEntity;
import com.surabi.adminservice.exception.RestaurantCustomException;
import com.surabi.adminservice.repository.AuthoritiesRepository;
import com.surabi.adminservice.repository.UserRepository;
import com.surabi.adminservice.service.IUserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("/api/v1")
public class UserController{

    @Autowired
    IUserService iUserService;
    @Autowired
    UserRepository userRepository;
    @Autowired
    AuthoritiesRepository authoritiesRepository;

    //read
    @GetMapping("/users")
    @ApiOperation(value = "Get All UserEntity", notes = "Fetch all UserEntity as list")
    public List<UserEntity> getAllUsers() throws RestaurantCustomException{
        return userRepository.findAll();
    }
    @GetMapping("/user")
    @ApiOperation(value = "Get a user by id", notes = "Fetch specific user")
    public Optional<UserEntity> getAUserById(Long id) throws RestaurantCustomException{
        return userRepository.findById(id);
    }

    //add
    @PostMapping("/user")
    @ApiOperation(value = "Add a new user", notes = "Add a specific user")
    public void addAUser(UserEntity userEntity) throws RestaurantCustomException{
        userEntity.setEnabled(true);
        iUserService.addAUser(userEntity);
    }

    //delete
    @DeleteMapping("/user")
    @ApiOperation(value = "Delete a user", notes = "Remove specific user by id")
    public void deleteUserById(Long id) throws RestaurantCustomException{
        iUserService.deleteUserById(id);
    }
    @DeleteMapping("/users")
    @ApiOperation(value = "Prune Table", notes = "Remove all records from user table")
    public void deleteAllUsers() throws RestaurantCustomException {
        iUserService.deleteAllUsers();
    }

    //update
    @PutMapping("/user")
    public void updateUserNameById(String userName, Long id) throws RestaurantCustomException {
        Optional<UserEntity> currentUser =  userRepository.findById(id);
        if(currentUser.isPresent()) {
            try {
                UserEntity user = currentUser.get();
                user.setUsername(userName);
                userRepository.saveAndFlush(user);
                Optional<AuthoritiesEntity> authoritiesEntity = authoritiesRepository.findById(user.getUsername());
                AuthoritiesEntity authority = authoritiesEntity.get();
                authority.setUsername(userName);
                authoritiesRepository.save(authority);
            }
            catch (Exception ex){
                throw new RestaurantCustomException("Update execution failed", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        else {
            throw new RestaurantCustomException("User does not exist for given id ", HttpStatus.NOT_FOUND);
        }
    }

}
