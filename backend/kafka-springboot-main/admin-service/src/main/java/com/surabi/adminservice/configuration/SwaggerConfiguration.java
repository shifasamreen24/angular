package com.surabi.adminservice.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfiguration {

    @Bean
    public Docket surabiRestaurantApi(){
        return new Docket(DocumentationType.SWAGGER_2).apiInfo(info()).groupName("Surabi-API")
                .select().apis(RequestHandlerSelectors.basePackage("com.surabi.adminservice.controller"))
                .build();
    }

    public ApiInfo info(){
        return new ApiInfoBuilder()
                .title("Surabi Admin API")
                .contact(new Contact("Restaurant API", "https://surabi-group.com", "surabi_restaurant@gmail.com"))
                .description("Surabi Restaurant Admin Service")
                .termsOfServiceUrl("https://surabi-group.com/toc")
                .version("1.0.0")
                .license("Surabi Group License").licenseUrl("https://surabi-group.com/license")
                .build();
    }
}
