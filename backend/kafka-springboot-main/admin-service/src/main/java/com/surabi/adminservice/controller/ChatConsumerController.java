package com.surabi.adminservice.controller;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/api/v1")
public class ChatConsumerController {

    @Autowired
    private Gson gson;

    @KafkaListener(topics = {"online-user-admin-topic"})
    public void receiveMessagesFromOnlineUser(@RequestParam String messageFromOnlineUser){
        log.info("Msg received from Kafka is: {}", messageFromOnlineUser);
        String message = gson.toJson(messageFromOnlineUser, String.class);
        log.info("Msg converted using gson: {}", message);
    }

    @KafkaListener(topics = {"offline-user-admin-topic"})
    public void receiveMessagesFromOfflineUser(@RequestParam String messageFromOfflineUser){
        log.info("Msg received from Kafka is: {}", messageFromOfflineUser);
        String message = gson.toJson(messageFromOfflineUser, String.class);
        log.info("Msg converted using gson: {}", message);
    }

}
