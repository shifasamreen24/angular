import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit, Optional } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { User } from './user';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  public users: User[] = [];
  
  constructor(private userService: UserService){}
  ngOnInit(): void {
    this.getAllUsers();
  }

  public getAllUsers(): void {
    this.userService.getAllUsers().subscribe(
      (response: User[]) => {
        this.users = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
    );
  }

  public onAddUser(addForm: NgForm): void {

    document.getElementById('add-user-form')?.click();
    this.userService.createUser(addForm.value).subscribe(
        (response: User) =>{},
        (error: HttpErrorResponse) => {
          alert(error.message);
        }
      );
  }

  public openModal(user: User | null, mode: string): void{

    const container = document.getElementById('main-container');

    const button = document.createElement('button');
    button.type = 'button';
    button.style.display = 'none';
    button.setAttribute('data-toggle','modal');

    if(mode === 'add'){
      button.setAttribute('data-target', '#addUserModal');
    }
    if(mode === 'update'){
      button.setAttribute('data-target', '#updateUserModal');
    }
    if(mode === 'delete'){
      button.setAttribute('data-target', '#deleteUserModal');
    }

    container?.appendChild(button);
    button.click();
  }
}
