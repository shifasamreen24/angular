import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { User } from "./user"; 

@Injectable({
    providedIn: 'root'
})
export class UserService {

    private apiServerUrl = environment.apiBaseUrl;

    constructor(private http: HttpClient) {}
 
    // register
    public login(): Observable<any>{
        return this.http.get<any>(`${this.apiServerUrl}/register`);
    }

    public getUser(id: number): Observable<User>{
        const options = {
            headers: new HttpHeaders({'Content-Type': 'application/json'}),
            queryParams: new HttpParams().set('id', JSON.stringify(id))
          };
        return this.http.get<any>(`${this.apiServerUrl}/user`);
    }
    public getAllUsers(): Observable<User[]>{
        return this.http.get<any>(`${this.apiServerUrl}/users`);
    }
    public createUser(user: User): Observable<User>{
        return this.http.post<User>(`${this.apiServerUrl}/user`, user);
    }
    public deleteUser(id: number): Observable<void>{
         const options = {
            headers: new HttpHeaders({'Content-Type': 'application/json'}),
            queryParams: new HttpParams().set('id', JSON.stringify(id))
          };         
        return this.http.delete<any>(`${this.apiServerUrl}/user`, options);
    }
    public updateUser(user: User): Observable<number>{
        return this.http.put<any>(`${this.apiServerUrl}/user`, user);
    }
}