import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Menu } from './menu';
import { MenuService } from './menu.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'userapp';
  public menus: Menu[] = [];
  public inputIds!: string;
  public selectedItemIds: number[]= [];
  public totalBillToDisplay: number = 0.0;

  constructor(private menuService : MenuService){}
 
  ngOnInit(): void {
    this.getMenu();
    
  }

  public getMenu(){

    this.menuService.getMenu().subscribe(
      (response: Menu[]) => {
        this.menus = response;
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
      );

  }

  // Total bill display
  public getTotalBill(ids: number[]){

    this.menuService.generateFinalBillByIdsAndCounts(ids).subscribe(
      (response: number) => {
        console.log("response "+response);
        this.totalBillToDisplay = response;
        console.log("Final bill amt received"+this.totalBillToDisplay);
      },
      (error: HttpErrorResponse) => {
        alert(error.message);
      }
      );
  }

  public generateBillFromInputIds(inputIds: string){
    console.log(inputIds);
    this.inputIds=inputIds;
    this.selectedItemIds = this.inputIds.split(",").map(Number);
    this.selectedItemIds.forEach(itemId => {
      if(this.selectedItemIds.indexOf(itemId) == -1){
        this.selectedItemIds.push();
      }
    });
    console.log("Selected ids"+this.selectedItemIds);
    this.getTotalBill(this.selectedItemIds);
    console.log("total bill"+this.totalBillToDisplay);
  }
}
