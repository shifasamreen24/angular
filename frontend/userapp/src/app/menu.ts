export interface Menu {

    id: number;
    item: string;
    price: number;
}