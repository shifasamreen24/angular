import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { Menu } from "./menu";

@Injectable({
    providedIn: 'root'
})
export class MenuService {

    private apiServerUrl = environment.apiBasueUrl;

    constructor(private http: HttpClient) {}
 
    // register
    public login(): Observable<any>{
        return this.http.get<any>(`${this.apiServerUrl}/register`);
    }

    // see all menu items
    public getMenu(): Observable<Menu[]>{
        return this.http.get<any>(`${this.apiServerUrl}/menu`);
    }
    //select 1 item by id (add item)
    public selectEachItemById(id: number): Observable<Menu>{
        return this.http.get<any>(`${this.apiServerUrl}/menu/item/${id}` );
    }
    //select many items by ids 
    public selectItemsByIdsAndCounts(ids: number[]): Observable<Menu[]>{
         //let queryParams = new HttpParams();
         const options = {
            headers: new HttpHeaders({'Content-Type': 'application/json'}),
            queryParams: new HttpParams().set('ids', JSON.stringify(ids))
          };         
        return this.http.get<any>(`${this.apiServerUrl}/menu/item`, options);
    }
    // get final bill on screen for list of item ids
    public generateFinalBillByIdsAndCounts(ids: number[]): Observable<number>{
        return this.http.post<any>(`${this.apiServerUrl}/placeOrder`, ids);
    }
}